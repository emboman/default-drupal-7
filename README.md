This is a default Drupal 7.25 install for NJ Web Developers, to be used as a starting point for doing Drupal development.

Some extra practices are taken into account with this Drupal instance, such as splitting up the modules folder by contributed, custom, and feature modules.

Feel free to fork this repository and use it as a template for your own Drupal development.
